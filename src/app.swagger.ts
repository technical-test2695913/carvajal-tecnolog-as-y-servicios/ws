import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const appSwagger = (app: INestApplication) => {
  const config = new DocumentBuilder()
    .setTitle('Red social Carvajal Tech')
    .setDescription('API para la Red social Carvajal Tech.')
    .setVersion('1.0.0')
    .addBearerAuth(undefined, 'defaultBearerAuth')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
};
