import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// Entities
import { IdentificationType } from './entities';

@Injectable()
export class IdentificationTypeService {
  constructor(
    @InjectRepository(IdentificationType)
    private readonly identificationTypeRepository: Repository<IdentificationType>,
  ) {}

  async getMany() {
    return await this.identificationTypeRepository.find();
  }

  async getOne(
    id: number,
    errorMsg = 'VAL_IDENTIFICATION_TYPE_NOT_EXIST_OR_UNAUTHORIZED',
  ) {
    const identificationType =
      await this.identificationTypeRepository.findOneBy({ id });

    if (!identificationType) throw new BadRequestException(errorMsg);

    return identificationType;
  }
}
