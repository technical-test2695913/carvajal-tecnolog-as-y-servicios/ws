import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Entities
import { IdentificationType } from './entities';

// Controllers
import { IdentificationTypeController } from './identification-type.controller';

// Services
import { IdentificationTypeService } from './identification-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([IdentificationType])],
  controllers: [IdentificationTypeController],
  providers: [IdentificationTypeService],
  exports: [IdentificationTypeService],
})
export class IdentificationTypeModule {}
