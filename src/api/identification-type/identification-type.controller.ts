import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

// Services
import { IdentificationTypeService } from './identification-type.service';

@ApiTags('Identification type routes')
@Controller('identification-type')
export class IdentificationTypeController {
  constructor(
    private readonly identificationTypeService: IdentificationTypeService,
  ) {}

  @Get()
  async getMany() {
    const data = await this.identificationTypeService.getMany();
    return { data };
  }
}
