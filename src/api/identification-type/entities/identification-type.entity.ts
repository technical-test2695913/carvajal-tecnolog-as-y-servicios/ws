import { PrimaryGeneratedColumn, Column, Entity, OneToOne } from 'typeorm';
import { IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

// Relations
import { User } from 'src/api/user/entities';

@Entity('identification-types')
export class IdentificationType {
  @ApiProperty({ example: 1, description: 'Id of the identification type' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 10, unique: true })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(2, 10, { message: 'VAL_MIN_MAX|2|10' })
  code: string;

  @Column({ type: 'varchar', length: 50, unique: true })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(1, 50, { message: 'VAL_MIN_MAX|1|50' })
  es: string;

  @Column({ type: 'varchar', length: 50, unique: true })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(1, 50, { message: 'VAL_MIN_MAX|1|50' })
  en: string;

  @OneToOne(() => User, (user) => user.identificationType, { cascade: true })
  users: User;
}
