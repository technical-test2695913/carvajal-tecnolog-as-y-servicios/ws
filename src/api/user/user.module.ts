import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Modules
import { GenderModule } from '../gender/gender.module';
import { IdentificationTypeModule } from '../identification-type/identification-type.module';

// Entities
import { User } from './entities';

// Controllers
import { UserController } from './user.controller';

// Services
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    GenderModule,
    IdentificationTypeModule
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule { }
