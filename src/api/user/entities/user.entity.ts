import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  Entity,
  ManyToOne,
  JoinColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';
import { hash } from 'bcryptjs';
import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';

// Relations
import { Gender } from 'src/api/gender/entities';
import { IdentificationType } from 'src/api/identification-type/entities';
import { Post } from 'src/api/post/entities';
@Entity('users', {
  orderBy: {
    createdAt: 'DESC',
  },
})
export class User {
  @ApiProperty({ example: 1, description: 'Id of the user' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  name: string;

  @Column({ name: 'last_name', type: 'varchar', length: 255 })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  lastName: string;

  @Column({ name: 'birth_day', type: 'date' })
  birthDay: Date;

  @ManyToOne(() => Gender, (gender) => gender.users, { eager: true })
  @JoinColumn({ name: 'gender' })
  gender: Gender;

  @ManyToOne(
    () => IdentificationType,
    (identificationType) => identificationType.users,
    { eager: true },
  )
  @JoinColumn({ name: 'identification_type' })
  identificationType: IdentificationType;

  @Column({ type: 'varchar', length: 20, nullable: true })
  @Length(8, 20, { message: 'VAL_MIN_MAX|8|20' })
  identification: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  @Length(3, 200, { message: 'VAL_MIN_MAX|3|200' })
  country: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  @Length(3, 200, { message: 'VAL_MIN_MAX|3|200' })
  city: string;

  @Column({ type: 'varchar', length: 500, nullable: true })
  @Length(3, 500, { message: 'VAL_MIN_MAX|3|500' })
  address: string;

  @Column({ type: 'varchar', length: 255, unique: true })
  @Length(3, 255, { message: 'VAL_MIN_MAX|8|255' })
  email: string;

  @Column({ type: 'varchar', length: 128, select: false })
  @Length(8, 128, { message: 'VAL_MIN_MAX|6|128' })
  password: string;

  @Column({ type: 'bool', default: true, nullable: true })
  active: boolean;

  @Column({ type: 'bool', default: false, nullable: true })
  verified: boolean;

  @Column({ type: 'bool', default: false, nullable: true })
  approved: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hash(this.password, 10);
  }

  @OneToOne(() => Post, (post) => post.user, { cascade: true })
  posts: Post;
}
