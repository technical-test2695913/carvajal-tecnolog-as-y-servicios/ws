import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  Length,
  Matches,
} from 'class-validator';

// Entties
import { Gender } from 'src/api/gender/entities';
import { IdentificationType } from 'src/api/identification-type/entities';

export class UserCreateDto {
  @ApiProperty({ example: 'Esteban', description: 'The name of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  name: string;

  @ApiProperty({ example: 'Diaz', description: 'The last name of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(3, 255, { message: 'VAL_MIN_MAX|3|255' })
  lastName: string;

  @ApiProperty({
    example: new Date('1994-03-18'),
    description: 'The birth day of the user',
  })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @IsDateString({ message: 'VAL_DATE' })
  birthDay: Date;

  @ApiProperty({ example: 1, description: 'The gender of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  gender: Gender;

  @ApiProperty({
    example: 1,
    description: 'The identification type of the user',
  })
  // @IsNotEmpty({ message: 'VAL_REQUIRED' })
  identificationType: IdentificationType;

  @ApiProperty({
    example: '1084934201',
    description: 'The identification of the user',
  })
  // @IsNotEmpty({ message: 'VAL_REQUIRED' })
  // @Length(8, 20, { message: 'VAL_MIN_MAX|8|20' })
  identification: string;

  @ApiProperty({ example: 'Colombia', description: 'The country of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  country: string;

  @ApiProperty({ example: 'Pasto', description: 'The city of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  city: string;

  @ApiProperty({
    example: 'Crr 14 #18A-40',
    description: 'The address of the user',
  })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(10, 500, { message: 'VAL_MIN_MAX|10|500' })
  address: string;

  @ApiProperty({
    example: 'stban94diaz@gmail.com',
    description: 'The email of the user',
  })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @IsEmail({}, { message: 'VAL_EMAIL' })
  email: string;

  @ApiProperty({ example: 'Hol@1234', description: 'The password of the user' })
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
    {
      message: 'VAL_PASSWORD',
    },
  )
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(8, 128, { message: 'VAL_MIN_MAX|8|128' })
  password: string;

  @ApiProperty({ example: 'es', description: 'The language of the email' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  lang: string;
}
