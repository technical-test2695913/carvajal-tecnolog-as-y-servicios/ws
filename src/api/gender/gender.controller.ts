import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

// Services
import { GenderService } from './gender.service';

@ApiTags('Gender routes')
@Controller('gender')
export class GenderController {
  constructor(private readonly genderService: GenderService) {}

  @Get()
  async getMany() {
    const data = await this.genderService.getMany();
    return { data };
  }
}
