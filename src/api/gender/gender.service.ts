import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// Entities
import { Gender } from './entities';

@Injectable()
export class GenderService {
  constructor(
    @InjectRepository(Gender)
    private readonly genderRepository: Repository<Gender>,
  ) { }

  async getMany() {
    return await this.genderRepository.find();
  }

  async getOne(id: number, errorMsg = 'VAL_GENDER_NOT_EXIST_OR_UNAUTHORIZED') {
    const gender = await this.genderRepository.findOneBy({ id });

    if (!gender) throw new BadRequestException(errorMsg);

    return gender;
  }
}
