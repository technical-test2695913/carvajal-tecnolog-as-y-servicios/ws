import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  JoinColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

// Relations
import { User } from 'src/api/user/entities';

@Entity('posts', {
  orderBy: {
    createdAt: 'DESC',
  },
})
export class Post {
  @ApiProperty({ example: 1, description: 'Id of the post' })
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 500 })
  text: string;

  @ManyToOne(() => User, (user) => user.posts, { eager: true })
  @JoinColumn({ name: 'user' })
  user: User;

  @ManyToOne(() => Post, post => post.answers)
  @JoinColumn({ name: 'inResponseTo' })
  inResponseTo: Post;

  @OneToMany(() => Post, post => post.inResponseTo)
  answers: Post[];

  @Column({ type: 'bool', default: true, nullable: true })
  active: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;
}
