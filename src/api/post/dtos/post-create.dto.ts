import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty, IsOptional
} from 'class-validator';

// Entties
import { User } from 'src/api/user/entities';
import { Post } from '../entities';

export class PostCreateDto {
  @ApiProperty({ example: 'Test post', description: 'The text of the post' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  text: string;

  @ApiProperty({ example: 1, description: 'The user of the post' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  user: User;

  @ApiProperty({ example: 1, description: 'The post that is answered' })
  @IsOptional()
  inResponseTo: Post;

  @ApiProperty({ example: 'es', description: 'The language of the email' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  lang: string;
}
