import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

// Entities
import { Post } from '../entities';

export class PostEditDto {
  @ApiProperty({ example: 'Test post', description: 'The text of the post' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  text: string;

  @ApiProperty({ example: 1, description: 'The post that is answered' })
  @IsOptional()
  inResponseTo: Post;
}
