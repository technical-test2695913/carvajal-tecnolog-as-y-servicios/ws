import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Services
import { UserService } from '../user/user.service';

// Entities
import { Post } from './entities';

// Data transfer objects
import { PostCreateDto, PostEditDto } from './dtos';
import { objToSingleValue } from 'src/utils/obj-to-single-value';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    private readonly userService: UserService
  ) { }

  async getMany() {
    return await this.postRepository.find({
      where: { active: true },
      select: {
        id: true,
        text: true,
        inResponseTo: {
          text: true
        }
      },
      relations: ['inResponseTo'],
      loadRelationIds: true
    });
  }

  async getOne(id: number, errorMsg = 'VAL_POST_NOT_EXIST_OR_UNAUTHORIZED') {
    const post = await this.postRepository.findOne({
      where: { id },
      loadRelationIds: true,
    });

    if (!post) throw new NotFoundException(errorMsg);

    return post;
  }

  async create(dto: PostCreateDto) {
    await this.userService.getOne(
      objToSingleValue(dto.user),
      'VAL_USER_NOT_EXIST',
    );
    if (dto.inResponseTo) await this.getOne(
      objToSingleValue(dto.inResponseTo),
      'VAL_POST_NOT_EXIST',
    );
    if (dto.text.length < 10) throw new NotFoundException('VAL_POST_MIN');
    if (dto.text.length > 500) throw new NotFoundException('VAL_POST_MAX');

    const newPost = this.postRepository.create(dto);
    return await this.postRepository.save(newPost);
  }

  async edit(id: number, dto: PostEditDto) {
    await this.getOne(id, 'VAL_POST_NOT_EXIST');

    return await this.postRepository.update(id, dto);
  }

  async delete(id: number) {
    return await this.edit(id, { active: false } as any);
  }
}
