import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  UseGuards,
  Headers,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

// Services
import { PostService } from './post.service';

// Data transfer objects
import { PostCreateDto, PostEditDto } from './dtos';

// Guards
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiTags('Post routes')
@ApiBearerAuth('defaultBearerAuth')
@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) { }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'List post' })
  async getMany() {
    const data = await this.postService.getMany();
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiOperation({ summary: 'Get post by id' })
  async getOne(@Param('id') id: number) {
    const data = await this.postService.getOne(id);
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({ summary: 'Create post' })
  async create(@Body() dto: PostCreateDto, @Headers() headers: any) {
    const data = await this.postService.create(dto);
    return { message: 'POST_CREATED', data };
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @ApiOperation({ summary: 'Update post by id (Any post atributes)' })
  async edit(@Param('id') id: number, @Body() dto: PostEditDto) {
    return {
      message: 'POST_EDITED',
      data: !!(await this.postService.edit(id, dto)).affected,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiOperation({ summary: 'Delete post' })
  async deleteOne(@Param('id') id: number) {
    return {
      message: 'POST_DELETED',
      data: !!(await this.postService.delete(id)).affected,
    };
  }
}
