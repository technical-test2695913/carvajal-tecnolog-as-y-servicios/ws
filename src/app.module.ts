import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

// Config DB
import databaseConfig from './config/db-config';

// Modules
import { AuthModule } from './auth/auth.module';
import { UserModule } from './api/user/user.module';
import { GenderModule } from './api/gender/gender.module';
import { IdentificationTypeModule } from './api/identification-type/identification-type.module';
import { PostModule } from './api/post/post.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'),
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) =>
        config.get<TypeOrmModuleOptions>('database.config'),
    }),
    ConfigModule.forRoot({
      load: [databaseConfig],
      envFilePath:
        process.env.ENV === 'prod' ? '.production.env' : '.development.env',
      isGlobal: true,
    }),
    AuthModule,
    UserModule,
    GenderModule,
    IdentificationTypeModule,
    PostModule
  ],
})
export class AppModule { }
