import { JwtService, JwtSignOptions } from '@nestjs/jwt';

export const getToken = (payload: any, opts: JwtSignOptions = {}) => {
  return new JwtService({ secret: process.env.JWT_SECRET }).sign(payload, {
    ...opts,
  });
};

export const verifyToken = (token: string) => {
  try {
    return new JwtService({ secret: process.env.JWT_SECRET }).verify(token);
  } catch (error) {
    return null;
  }
};
