import { QueryRunner } from "typeorm";

export const addMany = async (repository: any, rows = []) => {
  for (const row of rows) {
    const rowExist = await repository.findOneBy({ code: row.code });
    if (!rowExist) {
      await repository.save(row);
    }
  }
};

export const addManyMigration = async (qr: QueryRunner, data = []) => {
  for await (const item of data) {
    qr.query(`
      INSERT INTO users
        (${Object.keys(item).join(', ')})
      VALUES
        (${Object.keys(item)
        .map(k => (typeof item[k] === 'string') ? `'${item[k]}'` : item[k])
        .join(', ')})`);
  }
}
