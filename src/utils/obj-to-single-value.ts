export const objToSingleValue = (obj: any, key = 'id') => {
  if (obj && obj[key]) {
    return obj[key];
  }
  return obj;
};
