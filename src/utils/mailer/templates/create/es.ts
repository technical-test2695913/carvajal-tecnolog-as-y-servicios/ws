import { EmailContent } from 'src/types/email-content';

export const es: EmailContent = {
  subject: 'Verificación de usuario',
  html: `
    <center>
      <h1>Verificación de usuario</h1>
      <h4><i>arg0</i> - arg1</h4>
      <br/><br/>
      <p align="justify">
        lorem ipsum dolor sit amet, consectetur adipiscing elit. lorem ipsum dolor sit amet, consectetur adipiscing elit.
        lorem ipsum dolor sit amet, consectetur adipiscing elit. lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </p>
      <a href="arg2">Verificar</a>
    </center>
  `,
};
