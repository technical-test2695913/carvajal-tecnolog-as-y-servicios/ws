import { EmailContent } from 'src/types/email-content';

export const en: EmailContent = {
  subject: 'Change password',
  html: `
    <center>
      <h1>Change password</h1>
      <br/><br/>
      <p align="justify">
        lorem ipsum dolor sit amet, consectetur adipiscing elit. lorem ipsum dolor sit amet, consectetur adipiscing elit.
        lorem ipsum dolor sit amet, consectetur adipiscing elit. lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </p>
      <a href="arg0">Go to change the password</a>
    </center>
  `,
};
