import { createTestAccount, createTransport } from 'nodemailer';

export const connEthereal = async () => {
  const testAccount = await createTestAccount();

  return createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  });
};
