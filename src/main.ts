import { NestFactory } from '@nestjs/core';

// App Module
import { AppModule } from './app.module';

// Configurations
import { appSwagger } from './app.swagger';
import { appClassValidator } from './app.class-validator';

// Utils
import { logger } from './utils/logger';
import { globals } from './utils/globals';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix('api');

  appClassValidator(app);
  appSwagger(app);

  await app.listen(process.env.PORT || 3000);
  globals.appURL = await app.getUrl();
  logger(`Application is running on: ${globals.appURL}`);
  logger(`Swagger is running on: ${await app.getUrl()}/api`);
}

bootstrap();
