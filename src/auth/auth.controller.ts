import { Controller, Post, UseGuards, Body, Headers } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

// Guards
import { LocalAuthGuard } from './local-auth.guard';

// Custom decorators
import { User } from 'src/common/decorators/user';

// Services
import { AuthService } from './auth.service';

// Entities
import { User as UserEntity } from 'src/api/user/entities';

// DTOs
import { UserLoginDto } from './dtos';
import { ChangePassApplyDto, ChangePassDto } from './dtos/change-pass.dto';

// Utils
import { globals } from 'src/utils/globals';

@ApiTags('Auth routes')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) { }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@Body() loginDto: UserLoginDto, @User() user: UserEntity) {
    return this.authService.login(user);
  }

  @Post('/change-pass-apply')
  async changePassApply(
    @Body() dto: ChangePassApplyDto,
    @Headers() headers: any,
  ) {
    globals.appURL = headers.origin;
    return this.authService.changePassApply(dto);
  }

  @Post('/change-pass')
  async changePass(@Body() dto: ChangePassDto) {
    return {
      message: 'VAL_PASS_CHANGED',
      data: !!(await this.authService.changePass(dto)).affected,
    };
  }
}
