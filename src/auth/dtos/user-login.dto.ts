import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length, Matches } from 'class-validator';

export class UserLoginDto {
  @ApiProperty({
    example: 'stban94diaz@gmail.com',
    description: 'The email of the user',
  })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @IsEmail({}, { message: 'VAL_EMAIL' })
  email: string;

  @ApiProperty({ example: 'Hol@1234', description: 'The password of the user' })
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
    {
      message: 'VAL_PASSWORD',
    },
  )
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(8, 128, { message: 'VAL_MIN_MAX|8|128' })
  password: string;
}
