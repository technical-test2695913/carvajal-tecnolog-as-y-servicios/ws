import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsJWT, IsNotEmpty, Length, Matches } from 'class-validator';

export class ChangePassApplyDto {
  @ApiProperty({
    example: 'stban94diaz@gmail.com',
    description: 'The email of the user',
  })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @IsEmail({}, { message: 'VAL_EMAIL' })
  email: string;

  @ApiProperty({ example: 'es', description: 'The language of the email' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  lang: string;
}

export class ChangePassDto {
  @ApiProperty({ example: 'xxx.xxx.xxx', description: 'The token of the user' })
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @IsJWT({ message: 'VAL_JWT' })
  changePassToken: string;

  @ApiProperty({ example: 'Hol@1234', description: 'The password of the user' })
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
    {
      message: 'VAL_PASSWORD',
    },
  )
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(8, 128, { message: 'VAL_MIN_MAX|8|128' })
  password: string;

  @ApiProperty({
    example: 'Hol@1234',
    description: 'The confirm password of the user',
  })
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
    {
      message: 'VAL_PASSWORD',
    },
  )
  @IsNotEmpty({ message: 'VAL_REQUIRED' })
  @Length(8, 128, { message: 'VAL_MIN_MAX|8|128' })
  confirmPassword: string;
}
