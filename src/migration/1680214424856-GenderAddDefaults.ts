import { MigrationInterface, QueryRunner } from "typeorm"

export class GenderAddDefaults1680214424856 implements MigrationInterface {
    data = [
        { code: 'M', es: 'Masculino', en: 'Male' },
        { code: 'F', es: 'Femenino', en: 'Female' },
        { code: 'O', es: 'Otro', en: 'Other' },
    ]

    public async up(queryRunner: QueryRunner): Promise<void> {
        for await (const item of this.data) {
            queryRunner.query(`
                INSERT INTO genders
                    (${Object.keys(item).join(', ')})
                SELECT
                    ${Object.keys(item)
                    .map(k => (typeof item[k] === 'string') ? `'${item[k]}'` : item[k])
                    .join(', ')}
                WHERE NOT EXISTS (
                    SELECT 1 FROM genders WHERE "code" = '${item.code}'
                )`);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> { }

}

