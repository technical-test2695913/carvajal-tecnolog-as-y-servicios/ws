import { MigrationInterface, QueryRunner } from "typeorm"

export class IdentificationTypeAddDefaults1680230885304 implements MigrationInterface {
    data = [
        { code: 'CC', es: 'Cédula de ciudadanía', en: 'Citizenship card' },
        { code: 'TI', es: 'Tarjeta de identidad', en: 'Identity card' },
        { code: 'CE', es: 'Cédula de extranjería', en: 'Foreigner ID' },
    ]

    public async up(queryRunner: QueryRunner): Promise<void> {
        for await (const item of this.data) {
            queryRunner.query(`
                INSERT INTO "identification-types"
                    (${Object.keys(item).join(', ')})
                SELECT
                    ${Object.keys(item)
                    .map(k => (typeof item[k] === 'string') ? `'${item[k]}'` : item[k])
                    .join(', ')}
                WHERE NOT EXISTS (
                    SELECT 1 FROM "identification-types" WHERE "code" = '${item.code}'
                )`);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> { }

}
