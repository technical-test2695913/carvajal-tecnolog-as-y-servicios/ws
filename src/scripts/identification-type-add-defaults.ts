import { getRepository } from 'typeorm';

import { addMany } from '../utils/add-may';

// Entities
import { IdentificationType } from 'src/api/identification-type/entities';

export const identificationTypeAddDefaults = async () => {
  await addMany(getRepository<IdentificationType>(IdentificationType), [
    { code: 'CC', es: 'Cédula de ciudadanía', en: 'Citizenship card' },
    { code: 'TI', es: 'Tarjeta de identidad', en: 'Identity card' },
    { code: 'CE', es: 'Cédula de extranjería', en: 'Foreigner ID' },
  ]);
};
