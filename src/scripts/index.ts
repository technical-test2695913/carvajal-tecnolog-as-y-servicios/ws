import { genderAddDefaults } from './gender-add-defaults';
import { identificationTypeAddDefaults } from './identification-type-add-defaults';

export const runScripts = async () => {
  await genderAddDefaults();
  await identificationTypeAddDefaults();
};
