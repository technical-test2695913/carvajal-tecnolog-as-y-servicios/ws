import { getRepository, MigrationInterface, QueryRunner, Repository } from 'typeorm';

import { addMany } from '../utils/add-may';

// Entities
import { Gender } from 'src/api/gender/entities';

export const genderAddDefaults = async () => {
  await addMany(getRepository<Gender>(Gender), [
    { code: 'M', es: 'Masculino', en: 'Male' },
    { code: 'F', es: 'Femenino', en: 'Female' },
    { code: 'O', es: 'Otro', en: 'Other' },
  ]);
};
