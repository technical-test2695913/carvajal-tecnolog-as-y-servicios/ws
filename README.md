## Descripción

Esta es una API REST elaborada para la gestion de usuarios, post y replicas a estos, para su ejecucion por favor de seguir los sigueintes pasos.

## Base de datos

En este caso se hizo uso de PostgreSQL, como motor de base de datos debido a que es muy robusto, mucho mas que por ejemplo MongoDB porque es una base de datos relacional que utiliza SQL para almacenar y recuperar datos, lo que significa que puede definir tablas para publicaciones con campos específicos para cada uno y garantizar la integridad de los datos, PostgreSQL también es mejor para manejar transacciones complejas y consultas de unión. Además, PostgreSQL tiene una arquitectura monolítica que lo hace más fácil de administrar y mantener.

Se deja aqui un archivo llamado `docker-compose.yml` para que lo use en el caso de que quiera crear una instancia de base de datos postgres con las credenciales del archivo de ejemplo del las variables de entorno para su comodidad.

## Variables de entorno

Debe agregar un archivo llamado `.development.env` el cual debe tener la estructura del archivo `.copy.env`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### Importante

En caso de que la aplicación genere problemas al ejecutar las migraciones por favor dirijase al archivo que esta en `src/config/db-config.ts` y comentaree las lineas 16 y 17, guarda para que la aplicación compile, luego de que compile la aplicación, las descomentarea y nuevamente vuelve a guardar, con esto ya debe funcionar correctamente.